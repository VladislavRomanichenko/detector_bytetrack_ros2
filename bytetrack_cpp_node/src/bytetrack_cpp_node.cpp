#include "bytetrack_cpp_node/bytetrack_cpp_node.hpp"
#include "bytetrack_cpp_node/coco_names.hpp"

namespace bytetrack_cpp_node{
    using namespace bytetrack_cpp;

    std::vector<Object> BoundingBoxes2Objects(const std::vector<objects_msgs::msg::Object2D> bboxes)
    {
        std::vector<Object> objects;
        float scale = 1.0;
        for(auto bbox: bboxes){
            Object obj;
            obj.rect.x = bbox.x / scale;
            obj.rect.y = bbox.y / scale;
            obj.rect.width = bbox.width;
            obj.rect.height = bbox.height;
            obj.label = bbox.label;
            obj.prob = bbox.score;
            objects.push_back(obj);
        }
        return objects;
    }
    std::vector<objects_msgs::msg::Object2D> STrack2BoundingBoxes(const std::vector<STrack> trackers)
    {
        std::vector<objects_msgs::msg::Object2D> bboxes;
        for(int i=0; i<trackers.size(); i++){
            objects_msgs::msg::Object2D bbox;
            bbox.x = trackers[i].tlbr[0];
            bbox.y = trackers[i].tlbr[1];
            bbox.id = trackers[i].track_id;
            bbox.label = trackers[i].label;
            
            // bbox.center_dist = ;
            
            bbox.height = trackers[i].tlbr[3] - trackers[i].tlbr[1];
            bbox.width =  trackers[i].tlbr[2] - trackers[i].tlbr[0];

            bboxes.push_back(bbox);
        }
        return bboxes;
    }
    
    ByteTrackNode::ByteTrackNode(const std::string &node_name, const rclcpp::NodeOptions& options)
    : rclcpp::Node("bytetrack_cpp_node", node_name, options)
    {
    }
    ByteTrackNode::ByteTrackNode(const rclcpp::NodeOptions& options)
    : ByteTrackNode("", options)
    {
        this->initializeParameter_();
        this->tracker_ = std::make_unique<BYTETracker>(this->video_fps_, this->track_buffer_);

        this->sub_bboxes_ = this->create_subscription<objects_msgs::msg::Object2DArray>(
                            this->sub_bboxes_topic_name_, 10, 
                            std::bind(&ByteTrackNode::topic_callback_, 
                                      this,
                                      std::placeholders::_1));
        this->pub_bboxes_ = this->create_publisher<objects_msgs::msg::Object2DArray>(
            this->pub_bboxes_topic_name_,
            10
        );
    }
    void ByteTrackNode::initializeParameter_()
    {
        this->video_fps_ = this->declare_parameter<int>("video_fps", 30);
        this->track_buffer_ = this->declare_parameter<int>("track_buffer", 30);
        this->sub_bboxes_topic_name_ = this->declare_parameter<std::string>("sub_bboxes_topic_name", "/detection");
        this->pub_bboxes_topic_name_ = this->declare_parameter<std::string>("pub_bboxes_topic_name", "/bounding_boxes");
    }
    void ByteTrackNode::topic_callback_(const objects_msgs::msg::Object2DArray::ConstSharedPtr msg)
    {
        objects_msgs::msg::Object2DArray bboxes;
        bboxes.header = msg->header;

        vector<Object> objects = BoundingBoxes2Objects(msg->objects);
        vector<STrack> output_stracks = this->tracker_->update(objects);
        RCLCPP_INFO(this->get_logger(), "Detect objects: %d, Output Tracker: %d", objects.size(), output_stracks.size());
        bboxes.objects = STrack2BoundingBoxes(output_stracks);
        this->pub_bboxes_->publish(bboxes);
    }
}

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::NodeOptions node_options;
  rclcpp::spin(std::make_shared<bytetrack_cpp_node::ByteTrackNode>(node_options));
  rclcpp::shutdown();
  return 0;
}

#include <rclcpp_components/register_node_macro.hpp>
RCLCPP_COMPONENTS_REGISTER_NODE(bytetrack_cpp_node::ByteTrackNode)
