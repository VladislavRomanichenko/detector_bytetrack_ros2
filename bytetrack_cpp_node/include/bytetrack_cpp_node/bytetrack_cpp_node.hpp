#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <numeric>
#include <chrono>
#include <vector>
#include <dirent.h>

#include <rclcpp/rclcpp.hpp>
#include <objects_msgs/msg/object2_d.hpp>
#include <objects_msgs/msg/object2_d_array.hpp>

#include "bytetrack_cpp/BYTETracker.h"

namespace bytetrack_cpp_node{
    using namespace bytetrack_cpp;

    class ByteTrackNode : public rclcpp::Node
    {
    public:
        ByteTrackNode(const std::string &node_name, const rclcpp::NodeOptions& options);
        ByteTrackNode(const rclcpp::NodeOptions& options);

    private:
        void initializeParameter_();
        void topic_callback_(const objects_msgs::msg::Object2DArray::ConstSharedPtr msg);
        
        int video_fps_ = 30;
        int track_buffer_ = 30;
        std::string sub_bboxes_topic_name_;
        std::string pub_bboxes_topic_name_;
        std::unique_ptr<BYTETracker> tracker_;

        rclcpp::Publisher<objects_msgs::msg::Object2DArray>::SharedPtr pub_bboxes_;
        rclcpp::Subscription<objects_msgs::msg::Object2DArray>::SharedPtr sub_bboxes_;
    };
}