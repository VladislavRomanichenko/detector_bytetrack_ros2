This package is a port of [ByteTrack](https://github.com/ifzhang/ByteTrack) and [fateshelled/bytetrack_ros](https://github.com/fateshelled/bytetrack_ros) to ROS2.
# Requirements:

- ROS2 Foxy/Humble
- OpenCV
- [_cv_bridge_](https://answers.ros.org/question/262329/how-to-install-cv_bridge/)
- TensorRT and  [CUDA](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#download-the-nvidia-cuda-toolkit)
- [objects_msgs](https://gitlab.com/dtolkachev/objects_msgs/-/tree/ros2?ref_type=heads)
---

# Setup:

```
git clone https://gitlab.com/dtolkachev/objects_msgs.git
git clone https://github.com/PugBuffy/ros2_detect_package.git
git clone https://gitlab.com/VladislavRomanichenko/detector_bytetrack_ros2.git

colcon build

```
--- 

# Run:
```
cd ~/
source /opt/ros/foxy/setup.bash
source ./install/setup.bash

ros2 run bytetrack_cpp_node bytetrack_cpp_node
```
